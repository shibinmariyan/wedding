"use strict";

function makeTimer() {
  var startDate = Date.parse(new Date()) / 1000,
      endTime = Date.parse(new Date("Jan 17 2022 ,11:00 AM")) / 1000;
  var timeLeft = endTime - startDate,
      days = Math.floor(timeLeft / 86400),
      hours = Math.floor((timeLeft - days * 86400) / 3600),
      minutes = Math.floor((timeLeft - days * 86400 - hours * 3600) / 60),
      seconds = Math.floor(timeLeft - days * 86400 - hours * 3600 - minutes * 60);

  if (hours < 10) {
    hours = "0" + hours;
  }

  if (minutes < 10) {
    minutes = "0" + minutes;
  }

  if (seconds < 10) {
    seconds = "0" + seconds;
  }

  if (days > 0) $("#days").html(days + "<span>Days</span>");
  if (days > 0 && hours > 0) $("#hours").html(hours + "<span>Hours</span>");
  $("#minutes").html(minutes + "<span>Min</span>");
  $("#seconds").html(seconds + "<span>Sec</span>");
}

setInterval(function () {
  makeTimer();
}, 1000);